import logging
import os
import crypt
import random
import re
import shutil
import string
from pathlib import Path

logger = logging.getLogger()


class DeviceNotFoundError(Exception):
    pass


class DevicePartitionHandler:
    MOUNT_PATH: Path = Path("./media")

    @property
    def PARTITION_LABEL(self):
        raise NotImplementedError

    def __init__(self, device_path: str):
        self.device_path: Path = Path(device_path)
        if not self.device_path.exists():
            raise DeviceNotFoundError(
                "Pleas make sure you specified "
                "the correct path to the device.")
        self._partition_path = None

    @property
    def partition_path(self):
        if self._partition_path:
            return self._partition_path
        logger.debug(
            f"Searching partition name for partition labeled "
            f"{self.PARTITION_LABEL} at {self.device_path} ..."
        )
        with os.popen(f"lsblk -o name,label {self.device_path}") as command:
            lsblk_output = command.read()
        extract_partition_regex = re.compile(f"([a-z0-9]+) {self.PARTITION_LABEL}")
        partition_name = extract_partition_regex.findall(lsblk_output)[0]
        logger.debug(f"Partition {self.PARTITION_LABEL} is at {partition_name}")
        self._partition_path = Path("/dev") / partition_name
        return self._partition_path

    @classmethod
    def _create_mount_folder(cls):
        cls.MOUNT_PATH.mkdir(parents=True)

    @classmethod
    def _delete_mount_folder(cls):
        shutil.rmtree(str(cls.MOUNT_PATH))

    def _mount(self):
        os.system(f"mount {self.partition_path} {self.MOUNT_PATH}")

    def _umount(self):
        os.system(f"umount {self.partition_path}")

    def actions(self, **kwargs):
        raise NotImplementedError()

    def set_up(self, **kwargs):
        logger.debug(f"Configuring {self.PARTITION_LABEL} partition ...")
        self._create_mount_folder()
        try:
            self._mount()
            try:
                self.actions(**kwargs)
            finally:
                self._umount()
        finally:
            self._delete_mount_folder()
        logger.debug(f"{self.PARTITION_LABEL} partition configured")


class FSPartitionHandler(DevicePartitionHandler):
    PARTITION_LABEL = "rootfs"
    SSH_KEYS_PATH = Path("./keys")
    PASSWORDS_FILE_PATH = Path("./passwords.txt")
    AUTH_KEYS = Path("home/pi/.ssh/authorized_keys")
    SSH_CONFIG_LOCAL = Path("./config_files/ssh_config")
    SSH_CONFIG_RBP = Path("/etc/ssh/ssh_config")
    WPA_SUPPLICANT_CONFIG_LOCAL = Path("./config_files/wpa_supplicant.conf")
    WPA_SUPPLICANT_CONFIG_RBP = Path("/etc/wpa_supplicant/wpa_supplicant.conf")

    def _create_hostname(self, hostname):
        with (self.MOUNT_PATH / 'etc/hostname').open('w+') as f:
            f.write(hostname)

    def _create_hosts(self, hostname):
        with (self.MOUNT_PATH / 'etc/hosts').open('w+') as f:
            f.write(f"127.0.0.1\tlocalhost\n"
                    f"::1\t\tlocalhost ip6-localhost ip6-loopback\n"
                    f"ff02::1\tip6-allnodes\nff02::2\tip6-allrouters\n\n"
                    f"127.0.1.1\t{hostname}\n")

    def _add_ssh_keys(self):
        if not self.SSH_KEYS_PATH.exists():
            return
        keys = self.SSH_KEYS_PATH.glob("*.pub")
        auth_keys_path = self.MOUNT_PATH / self.AUTH_KEYS
        if auth_keys_path.exists():
            return
        if keys:
            auth_keys_path.parents[0].mkdir()
        for key in keys:
            with auth_keys_path.open("a+") as f:
                f.write(key.read_text())

    def _set_passwd(self, hostname, password=None):
        """
        TODO: "Easy" to remember random passwords.
        """
        salt = ''.join(random.choices(
                string.ascii_lowercase + string.digits, k=10))
        store = False
        if not password:
            password = ''.join(random.choices(
                string.ascii_uppercase + string.digits, k=10))
            store = True
        hashed_password = crypt.crypt(password, f"$6${salt}")
        shadow_path = self.MOUNT_PATH / "etc/shadow"
        with shadow_path.open("r+") as f:
            shadow_content = f.read()
            f.seek(0)
            new_shadow = re.sub(
                r"pi:\$6\$.*\n",
                f"pi:{hashed_password}:17709:0:99999:7:::\n",
                shadow_content
            )
            f.write(new_shadow)
            f.truncate()
            if not store:
                return
            with self.PASSWORDS_FILE_PATH.open("a+") as fp:
                fp.write(f"{hostname}:{password}\n")
                fp.truncate()

    def _activate_agent_forwarding(self):
        shutil.copy(
            str(self.SSH_CONFIG_LOCAL),
            str(self.MOUNT_PATH / self.SSH_CONFIG_RBP)
        )

    def _add_config_wpa_supplicant(self):
        shutil.copy(
            str(self.WPA_SUPPLICANT_CONFIG_LOCAL),
            str(self.MOUNT_PATH / self.WPA_SUPPLICANT_CONFIG_RBP)
        )

    def actions(self, **kwargs):
        self._create_hostname(kwargs['hostname'])
        self._create_hosts(kwargs['hostname'])
        self._set_passwd(kwargs['hostname'], kwargs['password'])
        self._add_ssh_keys()
        self._add_config_wpa_supplicant()
        if not kwargs['no_agent_forwarding']:
            self._activate_agent_forwarding()


class BootPartitionHandler(DevicePartitionHandler):
    PARTITION_LABEL = "boot"

    def _enable_ssh(self):
        (self.MOUNT_PATH / "ssh").touch()

    def actions(self, **kwargs):
        self._enable_ssh()
