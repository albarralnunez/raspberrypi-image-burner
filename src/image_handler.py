import re
import urllib
from io import BytesIO
from time import sleep
from typing import Iterator

from pathlib import Path
import logging
from urllib.request import urlopen, Request
from zipfile import ZipFile

import os

logger = logging.getLogger()


class DownloadError(Exception):
    pass


class RaspbianImage:

    def __init__(self):
        self._path = None

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, value):
        self._path = value

    @property
    def sha256(self):
        if self._path:
            return self.path.name.split(".")[0]
        return

    def burn(self, device_path):
        command = f"dd if={self.path.absolute()} of={device_path} bs=32M"
        logger.debug("Burning SD ...")
        os.system(command)
        sleep(5)
        logger.debug("SD burned")


class RaspbianDownloader:

    PAGE_URL: str = "https://www.raspberrypi.org/downloads/raspbian/"
    PAGE_SHA256_REGEX = re.compile(
        r"SHA-256:</span> <strong>([a-z0-9]*)</strong></div>")
    IMG_PATH: Path = Path("./")
    IMG_URL: str = "https://downloads.raspberrypi.org/raspbian_lite_latest"

    def __init__(self, img: RaspbianImage):
        self.img: RaspbianImage = img
        self._remote_sha256 = None

    @property
    def remote_sha256(self):
        if self._remote_sha256:
            return self._remote_sha256
        req = Request(self.PAGE_URL, headers={"User-Agent": "Mozilla/5.0"})
        with urlopen(req) as url:
            content = url.read()
        html = content.decode()
        self._remote_sha256 = self.PAGE_SHA256_REGEX.findall(html)[1]
        return self._remote_sha256

    def is_updated(self):
        try:
            return self.img.sha256 == self.remote_sha256
        except urllib.error.URLError:
            if not self.img.path:
                raise DownloadError("Pleas make sure that internet connection is working")
            print("Connectivity problems is not possible to verify if there is an updated image")
            print("Do you want to proceed anyway?")
            value = input("[Y]/n")
            if value == "n" or value == "N":
                print("Aborted")
                exit(1)
            return False

    def find_img(self):
        dir_list: Iterator[Path] = self.IMG_PATH.glob("*.img")
        try:
            self.img.path = next(dir_list)
        except StopIteration:
            return None

    def download(self):
        self.find_img()
        if not self.is_updated():
            old_path = self.img.path
            logger.debug("Downloading ...")
            with urlopen(self.IMG_URL) as url:
                content = url.read()
            with ZipFile(BytesIO(content)) as zip_file:
                zip_file.extractall()
            logger.debug("Downloaded")
            self.find_img()
            self.img.path.rename(f"{self.remote_sha256}.img")
            if old_path:
                old_path.unlink()
        return self.img.path
