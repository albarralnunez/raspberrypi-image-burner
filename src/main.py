import logging

from src.device_handler import BootPartitionHandler, FSPartitionHandler
from src.image_handler import RaspbianImage, RaspbianDownloader

logger = logging.getLogger()


def main(args):
    logger.debug("Starting")
    img = RaspbianImage()
    downloader = RaspbianDownloader(img)
    downloader.download()
    if not args.no_burn:
        img.burn(device_path=args.sd)
    boot_handler = BootPartitionHandler(device_path=args.sd)
    boot_handler.set_up()
    fs_handler = FSPartitionHandler(device_path=args.sd)
    fs_handler.set_up(
        hostname=args.hostname,
        password=args.password,
        no_agent_forwarding=args.no_agent_forwarding
    )
    logger.debug("Finished")
